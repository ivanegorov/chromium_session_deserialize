#ifndef JSON_H
#define JSON_H

#include <iostream>
#include <map>
#include <string>
#include <utility>
#include <variant>
#include <vector>
#include <iomanip>

#include <type_traits>

namespace Json {

    template<typename T>
    struct is_number : std::conditional<(std::is_integral_v<T> || std::is_floating_point_v<T>) && !std::is_same_v<T, bool>, std::true_type, std::false_type>::type {};

    struct Node;
    using variant_inheritance = std::variant<uint64_t, int64_t, uint32_t, int, double, bool, std::string, std::u16string, std::vector<Node>, std::map<std::string, Node>>;

    struct Node : public Json::variant_inheritance {
    public:
        using variant_inheritance::variant_inheritance;

        [[nodiscard]] inline const Json::variant_inheritance &GetOrigin() const {
            return *this;
        }

        template<typename T>
        [[nodiscard]] inline std::enable_if_t<is_number<std::decay_t<T>>::value, T> AsNumber() const {
            return std::get<T>(*this);
        }

        [[nodiscard]] inline bool const &AsBool() const {
            return std::get<bool>(*this);
        }

        [[nodiscard]] inline std::string const &AsString() const {
            return std::get<std::string>(*this);
        }

        [[nodiscard]] inline std::u16string const &AsU16String() const {
            return std::get<std::u16string>(*this);
        }

        [[nodiscard]] inline std::vector<Node> const &AsArray() const {
            return std::get<std::vector<Node>>(*this);
        }

        [[nodiscard]] inline std::map<std::string, Node> const &AsMap() const {
            return std::get<std::map<std::string, Node>>(*this);
        }

        [[nodiscard]] inline std::vector<Node> &AsArray() {
            return std::get<std::vector<Node>>(*this);
        }

        [[nodiscard]] bool IsArray() const { return std::holds_alternative<std::vector<Node>>(*this); }

        [[nodiscard]] inline std::map<std::string, Node> &AsMap() {
            return std::get<std::map<std::string, Node>>(*this);
        }

        [[nodiscard]] bool IsMap() const { return std::holds_alternative<std::map<std::string, Node>>(*this); }

        [[nodiscard]] const Json::Node & operator[](std::string const & key) const {
            return AsMap().at(key);
        }

        [[nodiscard]] const Json::Node & operator[](size_t i) const {
            return AsArray().at(i);
        }

        [[nodiscard]] Json::Node & operator[](std::string const & key) {
            if (!IsMap()) {
                *this = std::map<std::string, Node>{};
            }
            return AsMap()[key];
        }

        [[nodiscard]] Json::Node & operator[](size_t i) {
            if (!IsArray()) {
                *this = std::vector<Node>{};
            }
            return AsArray()[i];
        }

        Json::Node & Insert(const std::string & key, Node const & node){
            if (!IsMap()) {
                *this = std::map<std::string, Node>{};
            }
            return std::get<std::map<std::string, Node>>(*this).insert_or_assign(key, node).first->second;
        }

        Json::Node & Push(Node const & node) {
            if (!IsArray()) {
                *this = std::vector<Node>{};
            }
            std::get<std::vector<Node>>(*this).push_back(node);
            return std::get<std::vector<Node>>(*this).back();
        }
    };

    struct Document {
    private:
        Node root;
    public:
        explicit Document(Node new_root) : root(std::move(new_root)) {};

        [[nodiscard]] inline Node const &GetRoot() const {
            return root;
        }
    };

    namespace Deserializer {
        Node LoadNode(std::istream &input);

        Node LoadMap(std::istream &input);

        Node LoadArray(std::istream &input);

        Node LoadString(std::istream &input);

        Node LoadNumber(std::istream &input);

        Node LoadBool(std::istream &input);
    }

    struct JsonServant {
        virtual Node MakeJson() = 0;
    };

    inline Document Load(std::istream &input) {
        return std::move(Document{Deserializer::LoadNode(input)});
    }

    namespace Serializer {
        template<typename T>
        std::enable_if_t<!is_number<T>::value, void> Serialize(const T &node, std::ostream &output = std::cout);

        template<>
        void Serialize<bool>(const bool &node, std::ostream &output);

        template<typename T>
        std::enable_if_t<is_number<T>::value, void> Serialize(const Json::Node &node, std::ostream &output) {
            output << node.template AsNumber<T>();
        }

        template<>
        void Serialize<std::string>(const std::string &node, std::ostream &output);

        template<>
        void Serialize<std::u16string>(const std::u16string &node, std::ostream &output);

        template<>
        void Serialize<std::vector<Node>>(const std::vector<Json::Node> &node, std::ostream &output);

        template<>
        void Serialize<std::map<std::string, Node>>(const std::map<std::string, Json::Node> &node, std::ostream &output);
    }
}

#endif //JSON_H