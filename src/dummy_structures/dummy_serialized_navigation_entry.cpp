#include "dummy_serialized_navigation_entry.h"

const int ObsoleteReferrerPolicyNever = 2;

enum TypeMask {
    HAS_POST_DATA = 1
};

DummySerializedNavigationEntry::DummySerializedNavigationEntry() {}

[[maybe_unused]] DummySerializedNavigationEntry::DummySerializedNavigationEntry(DummySerializedNavigationEntry &&other)  noexcept {
    operator=(std::move(other));
}

void WriteStringToPickle(DummyPickle* pickle,
                         int* bytes_written,
                         int max_bytes,
                         const std::string& str) {
    int num_bytes = str.size() * sizeof(char);
    if (*bytes_written + num_bytes < max_bytes) {
        *bytes_written += num_bytes;
        pickle->WriteString(str);
    } else {
        pickle->WriteString(std::string());
    }
}

void WriteString16ToPickle(DummyPickle* pickle,
                           int* bytes_written,
                           int max_bytes,
                           const std::u16string& str) {
    int num_bytes = str.size() * sizeof(char16_t);
    if (*bytes_written + num_bytes < max_bytes) {
        *bytes_written += num_bytes;
        pickle->WriteString16(str);
    } else {
        pickle->WriteString16(std::u16string());
    }
}

void DummySerializedNavigationEntry::WriteToPickle(int max_size, DummyPickle *pickle) const {
    pickle->WriteInt(index_);

    int bytes_written = 0;

    WriteStringToPickle(pickle, &bytes_written, max_size,
                        referrer_url_str_);

    WriteString16ToPickle(pickle, &bytes_written, max_size, title_);

    const std::string enc = encoded_page_state();
    WriteStringToPickle(pickle, &bytes_written, max_size, enc);

    pickle->WriteInt(transition_type_);

    const int type_mask = has_post_data_ ? HAS_POST_DATA : 0;
    pickle->WriteInt(type_mask);

    WriteStringToPickle(pickle, &bytes_written, max_size, referrer_url_str_);

    pickle->WriteInt(ObsoleteReferrerPolicyNever);

    WriteStringToPickle(
            pickle, &bytes_written, max_size,
            original_request_url_str_);
    pickle->WriteBool(is_overriding_user_agent_);
    pickle->WriteInt64(timestamp_);

    WriteString16ToPickle(pickle, &bytes_written, max_size, std::u16string());

    pickle->WriteInt(http_status_code_);

    pickle->WriteInt(referrer_policy_);

    pickle->WriteInt(extended_info_map_.size());
    for (const auto& entry : extended_info_map_) {
        WriteStringToPickle(pickle, &bytes_written, max_size, entry.first);
        WriteStringToPickle(pickle, &bytes_written, max_size, entry.second);
    }

    pickle->WriteInt64(task_id_);
    pickle->WriteInt64(parent_task_id_);
    pickle->WriteInt64(root_task_id_);

    pickle->WriteInt(0);
}

bool DummySerializedNavigationEntry::ReadFromPickle(PickleIterator *iterator) {
    *this = DummySerializedNavigationEntry();
    std::string virtual_url_spec;
    int transition_type_int = 0;
    if (!iterator->ReadInt(&index_) ||
        !iterator->ReadString(&virtual_url_spec) ||
        !iterator->ReadString16(&title_) ||
        !iterator->ReadString(&encoded_page_state_) ||
        !iterator->ReadInt(&transition_type_int))
        return false;
    virtual_url_str_ = virtual_url_spec;
    transition_type_ = transition_type_int;

    int type_mask = 0;
    bool has_type_mask = iterator->ReadInt(&type_mask);

    if (has_type_mask) {
        has_post_data_ = type_mask & HAS_POST_DATA;
        std::string referrer_spec;
        if (!iterator->ReadString(&referrer_spec))
            referrer_spec = std::string();
        referrer_url_str_ = referrer_spec;

        int ignored_referrer_policy;
        std::ignore = iterator->ReadInt(&ignored_referrer_policy);

        std::string original_request_url_spec;
        if (!iterator->ReadString(&original_request_url_spec))
            original_request_url_spec = std::string();
        original_request_url_str_ = original_request_url_spec;

        if (!iterator->ReadBool(&is_overriding_user_agent_))
            is_overriding_user_agent_ = false;

        int64_t timestamp_internal_value = 0;
        if (iterator->ReadInt64(&timestamp_internal_value)) {
            timestamp_ = timestamp_internal_value;
        } else {
            timestamp_ = 0;
        }

        std::u16string search_terms;
        std::ignore = iterator->ReadString16(&search_terms);

        if (!iterator->ReadInt(&http_status_code_))
            http_status_code_ = 0;

        int correct_referrer_policy;
        if (iterator->ReadInt(&correct_referrer_policy)) {
            referrer_policy_ = correct_referrer_policy;
        }

        int extended_info_map_size = 0;
        if (iterator->ReadInt(&extended_info_map_size) &&
            extended_info_map_size > 0) {
            for (int i = 0; i < extended_info_map_size; ++i) {
                std::string key;
                std::string value;
                if (iterator->ReadString(&key) && iterator->ReadString(&value))
                    extended_info_map_[key] = value;
            }
        }

        if (!iterator->ReadInt64(&task_id_))
            task_id_ = -1;

        if (!iterator->ReadInt64(&parent_task_id_))
            parent_task_id_ = -1;

        if (!iterator->ReadInt64(&root_task_id_))
            root_task_id_ = -1;

        int children_task_ids_size = 0;
        std::ignore = iterator->ReadInt(&children_task_ids_size);
    }

    is_restored_ = true;

    return true;
}

Json::Node MakeJson(const DummySerializedNavigationEntry & navigator) {
    Json::Node root;

    root.Insert("index", navigator.index_);
    root.Insert("unique id", navigator.unique_id_);
    root.Insert("referrer url str", navigator.referrer_url_str_);
    root.Insert("referrer policy", navigator.referrer_policy_);
    root.Insert("title", navigator.title_);
    root.Insert("virtual url str", navigator.virtual_url_str_);
    root.Insert("encoded page state", navigator.encoded_page_state_);
    root.Insert("timestamp", navigator.timestamp_);
    root.Insert("transition type", navigator.transition_type_);
    root.Insert("has post data", navigator.has_post_data_);
    root.Insert("post id ", navigator.post_id_);
    root.Insert("original request url str", navigator.original_request_url_str_);
    root.Insert("is overriding user agent", navigator.is_overriding_user_agent_);
    root.Insert("favicon url str", navigator.favicon_url_str_);
    root.Insert("http status code", navigator.http_status_code_);
    root.Insert("is restored", navigator.is_restored_);

    {
        auto &node = root.Insert("redirect chain", {});
        for (auto &el: navigator.referrer_url_str_)
            node.Push(el);
    }

    {
        auto &node = root.Insert("extended info map", {});
        for (auto &el: navigator.extended_info_map_)
            node.Insert(el.first, el.second);
    }

    root.Insert("task id", navigator.task_id_);
    root.Insert("parent task id", navigator.parent_task_id_);
    root.Insert("root task id", navigator.root_task_id_);

    return std::move(root);
}
