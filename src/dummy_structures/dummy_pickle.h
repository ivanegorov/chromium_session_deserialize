#ifndef SESSION_READER_DUMMY_PICKLE_H
#define SESSION_READER_DUMMY_PICKLE_H

#include <string>
#include <cstdint>
#include <optional>

#include "auxiliary.h"

class DummyPickle;

class PickleIterator {
public:
    PickleIterator() : payload_(nullptr), read_index_(0), end_index_(0) {}

    explicit PickleIterator(const DummyPickle &pickle);

    [[nodiscard]] bool ReadBool(bool *result);

    [[nodiscard]] bool ReadInt(int *result);

    [[nodiscard]] bool ReadLong(long *result);

    [[nodiscard]] bool ReadUInt16(uint16_t *result);

    [[nodiscard]] bool ReadUInt32(uint32_t *result);

    [[nodiscard]] bool ReadInt64(int64_t *result);

    [[nodiscard]] bool ReadUInt64(uint64_t *result);

    [[nodiscard]] bool ReadFloat(float *result);

    [[nodiscard]] bool ReadDouble(double *result);

    [[nodiscard]] bool ReadString(std::string *result);

    [[nodiscard]] bool ReadData(const char **data, int *length);

    [[nodiscard]] bool ReadBytes(const char **data, int length);

    [[nodiscard]] bool ReadLength(int *result) {
        return ReadInt(result) && *result >= 0;
    }

    [[nodiscard]] bool ReadString16(std::u16string* result);

    [[nodiscard]] bool SkipBytes(int num_bytes) {
        return !!GetReadPointerAndAdvance(num_bytes);
    }

    [[nodiscard]] bool ReachedEnd() const { return read_index_ == end_index_; }

private:

    template<typename Type>
    bool ReadBuiltinType(Type *result);

    void Advance(size_t size);

    template<typename Type>
    const char *GetReadPointerAndAdvance() {
        if (sizeof(Type) > end_index_ - read_index_) {
            read_index_ = end_index_;
            return nullptr;
        }
        const char* current_read_ptr = payload_ + read_index_;
        Advance(sizeof(Type));
        return current_read_ptr;
    }

    const char *GetReadPointerAndAdvance(int num_bytes) {
        if (num_bytes < 0 ||
            end_index_ - read_index_ < static_cast<size_t>(num_bytes)) {
            read_index_ = end_index_;
            return nullptr;
        }
        const char* current_read_ptr = payload_ + read_index_;
        Advance(num_bytes);
        return current_read_ptr;
    }

    const char *payload_;
    size_t read_index_;
    size_t end_index_;
};

struct DummyPickle {
public:
    DummyPickle();

    explicit DummyPickle(int header_size);

    DummyPickle(const char *data, size_t data_len);

    DummyPickle(const DummyPickle &other);

    virtual ~DummyPickle();

    DummyPickle &operator=(const DummyPickle &other);

    [[nodiscard]] size_t size() const {
        return header_ ? header_size_ + header_->payload_size : 0;
    }

    [[nodiscard]] const void *data() const { return header_; }

    [[nodiscard]] size_t GetTotalAllocatedSize() const;

    void WriteBool(bool value) { WriteInt(value ? 1 : 0); }

    void WriteInt(int value) { WritePOD(value); }

    void WriteLong(long value) {
        WritePOD(static_cast<int64_t>(value));
    }

    void WriteUInt16(uint16_t value) { WritePOD(value); }

    void WriteUInt32(uint32_t value) { WritePOD(value); }

    void WriteInt64(int64_t value) { WritePOD(value); }

    void WriteUInt64(uint64_t value) { WritePOD(value); }

    void WriteFloat(float value) { WritePOD(value); }

    void WriteDouble(double value) { WritePOD(value); }

    void WriteString(const std::string& value);

    void WriteString16(const std::u16string& value);

    void WriteData(const char *data, int length);

    void WriteBytes(const void *data, int length);

    void Reserve(size_t additional_capacity);

    struct Header {
        uint32_t payload_size;
    };

    [[nodiscard]] size_t payload_size() const {
        return header_ ? header_->payload_size : 0;
    }

    [[nodiscard]] const char* payload() const {
        return reinterpret_cast<const char*>(header_) + header_size_;
    }

protected:
    [[nodiscard]] size_t header_size() const { return header_size_; }

    char* mutable_payload() {
        return reinterpret_cast<char*>(header_) + header_size_;
    }

    [[nodiscard]] size_t capacity_after_header() const {
        return capacity_after_header_;
    }

    void Resize(size_t new_capacity);

    void* ClaimBytes(size_t num_bytes);

    static const char* FindNext(size_t header_size,
                                const char* range_start,
                                const char* range_end);

    static bool PeekNext(size_t header_size,
                         const char* range_start,
                         const char* range_end,
                         size_t* pickle_size);

    static const int PayloadUnit;

private:
    friend class PickleIterator;

    Header *header_;
    size_t header_size_;

    size_t capacity_after_header_;

    size_t write_offset_;

    template<size_t length> void WriteBytesStatic(const void* data) {
        WriteBytesCommon(data, length);
    }

    template <typename T> bool WritePOD(const T& data) {
        WriteBytesStatic<sizeof(data)>(&data);
        return true;
    }

    inline void* ClaimUninitializedBytesInternal(size_t num_bytes);
    inline void WriteBytesCommon(const void* data, size_t length);

};

struct DummyToken {
    uint64_t high;
    uint64_t low;
};

std::optional<DummyToken> ReadTokenFromPickle(PickleIterator* pickle_iterator);


#endif //SESSION_READER_DUMMY_PICKLE_H
