#include "dummy_pickle.h"

PickleIterator::PickleIterator(const DummyPickle &pickle) : payload_(pickle.payload()),
                                                            read_index_(0),
                                                            end_index_(pickle.payload_size()) {}

template<typename Type>
inline bool PickleIterator::ReadBuiltinType(Type *result) {
    const char *read_from = GetReadPointerAndAdvance<Type>();
    if (!read_from)
        return false;
    if (sizeof(Type) > sizeof(uint32_t))
        memcpy(result, read_from, sizeof(*result));
    else
        *result = *reinterpret_cast<const Type *>(read_from);
    return true;
}

inline void PickleIterator::Advance(size_t size) {
    size_t aligned_size = AlignUp(size, sizeof(uint32_t));
    if (end_index_ - read_index_ < aligned_size) {
        read_index_ = end_index_;
    } else {
        read_index_ += aligned_size;
    }
}

bool PickleIterator::ReadBool(bool *result) {
    return ReadBuiltinType(result);
}

bool PickleIterator::ReadInt(int *result) {
    return ReadBuiltinType(result);
}

bool PickleIterator::ReadLong(long *result) {
    return ReadBuiltinType(result);
}

bool PickleIterator::ReadUInt16(uint16_t *result) {
    return ReadBuiltinType(result);
}

bool PickleIterator::ReadUInt32(uint32_t *result) {
    return ReadBuiltinType(result);
}

bool PickleIterator::ReadInt64(int64_t *result) {
    return ReadBuiltinType(result);
}

bool PickleIterator::ReadUInt64(uint64_t *result) {
    return ReadBuiltinType(result);
}

bool PickleIterator::ReadFloat(float *result) {
    const char* read_from = GetReadPointerAndAdvance<float>();
    if (!read_from)
        return false;
    memcpy(result, read_from, sizeof(*result));
    return true;
}

bool PickleIterator::ReadDouble(double *result) {
    const char* read_from = GetReadPointerAndAdvance<double>();
    if (!read_from)
        return false;
    memcpy(result, read_from, sizeof(*result));
    return true;
}

bool PickleIterator::ReadString(std::string *result) {
    int len;
    if (!ReadInt(&len))
        return false;
    const char* read_from = GetReadPointerAndAdvance(len);
    if (!read_from)
        return false;

    result->assign(read_from, len);
    return true;
}

bool PickleIterator::ReadData(const char **data, int *length) {
    *length = 0;
    *data = nullptr;

    if (!ReadInt(length))
        return false;

    return ReadBytes(data, *length);
}

bool PickleIterator::ReadBytes(const char **data, int length) {
    const char* read_from = GetReadPointerAndAdvance(length);
    if (!read_from)
        return false;
    *data = read_from;
    return true;
}

bool PickleIterator::ReadString16(std::u16string *result) {
    int len;
    if (!ReadInt(&len))
        return false;
    const char* read_from = GetReadPointerAndAdvance(len * sizeof(char16_t));
    if (!read_from)
        return false;

    result->assign(reinterpret_cast<const char16_t*>(read_from), len);
    return true;
}

const int DummyPickle::PayloadUnit = 64;

static const size_t CapacityReadOnly = static_cast<size_t>(-1);

DummyPickle::DummyPickle() : header_(nullptr),
                             header_size_(sizeof(Header)),
                             capacity_after_header_(0),
                             write_offset_(0) {
    Resize(PayloadUnit);
    header_->payload_size = 0;
}

DummyPickle::DummyPickle(int header_size) : header_(nullptr),
                                            header_size_(AlignUp<int>(header_size, sizeof(uint32_t))),
                                            capacity_after_header_(0),
                                            write_offset_(0) {
    Resize(PayloadUnit);
    header_->payload_size = 0;
}
DummyPickle::DummyPickle(const char *data, size_t data_len) : header_(reinterpret_cast<Header*>(const_cast<char*>(data))),
                                                              header_size_(0),
                                                              capacity_after_header_(CapacityReadOnly),
                                                              write_offset_(0) {
    if (data_len >= static_cast<int>(sizeof(Header)))
        header_size_ = data_len - header_->payload_size;

    if (header_size_ > static_cast<unsigned int>(data_len))
        header_size_ = 0;

    if (header_size_ != AlignUp(header_size_, sizeof(uint32_t)))
        header_size_ = 0;

    if (!header_size_)
        header_ = nullptr;
}

DummyPickle::DummyPickle(const DummyPickle &other)  : header_(nullptr),
                                                      header_size_(other.header_size_),
                                                      capacity_after_header_(0),
                                                      write_offset_(other.write_offset_) {
    if (other.header_) {
        Resize(other.header_->payload_size);
        memcpy(header_, other.header_, header_size_ + other.header_->payload_size);
    }
}

DummyPickle::~DummyPickle() {
    if (capacity_after_header_ != CapacityReadOnly)
        free(header_);
}

DummyPickle &DummyPickle::operator=(const DummyPickle &other) {
    if (this == &other) {
        return *this;
    }
    if (capacity_after_header_ == CapacityReadOnly) {
        header_ = nullptr;
        capacity_after_header_ = 0;
    }
    if (header_size_ != other.header_size_) {
        free(header_);
        header_ = nullptr;
        header_size_ = other.header_size_;
    }
    if (other.header_) {
        Resize(other.header_->payload_size);
        memcpy(header_, other.header_,
               other.header_size_ + other.header_->payload_size);
        write_offset_ = other.write_offset_;
    }
    return *this;
}

void DummyPickle::WriteBytesCommon(const void *data, size_t length) {
    if (CapacityReadOnly != capacity_after_header_) {
        throw std::logic_error("oops: pickle is readonly");
    }
    void *write = ClaimUninitializedBytesInternal(length);
    memcpy(write, data, length);
}

void *DummyPickle::ClaimUninitializedBytesInternal(size_t length) {
    if (CapacityReadOnly != capacity_after_header_) {
        throw std::logic_error("oops: pickle is readonly");
    }
    size_t data_len = AlignUp(length, sizeof(uint32_t));
    if ((data_len < length) || (write_offset_ > std::numeric_limits<uint32_t>::max() - data_len)) {
        throw std::logic_error("oops: incorrect data length or write offset!");
    }
    size_t new_size = write_offset_ + data_len;
    if (new_size > capacity_after_header_) {
        size_t new_capacity = capacity_after_header_ * 2;
        const size_t PickleHeapAlign = 4096;
        if (new_capacity > PickleHeapAlign) {
            new_capacity =
                    AlignUp(new_capacity, PickleHeapAlign) - PayloadUnit;
        }
        Resize(std::max(new_capacity, new_size));
    }

    char *write = mutable_payload() + write_offset_;
    memset(write + length, 0, data_len - length);
    header_->payload_size = static_cast<uint32_t>(new_size);
    write_offset_ = new_size;
    return write;
}

size_t DummyPickle::GetTotalAllocatedSize() const {
    if (capacity_after_header_ == CapacityReadOnly)
        return 0;
    return header_size_ + capacity_after_header_;
}

void DummyPickle::WriteString(const std::string& value) {
    WriteInt(static_cast<int>(value.size()));
    WriteBytes(value.data(), static_cast<int>(value.size()));
}

void DummyPickle::WriteString16(const std::u16string &value) {
    WriteInt(static_cast<int>(value.size()));
    WriteBytes(value.data(), static_cast<int>(value.size()) * sizeof(char16_t));
}

void DummyPickle::WriteData(const char *data, int length) {
    WriteInt(length);
    WriteBytes(data, length);
}

void DummyPickle::WriteBytes(const void *data, int length) {
    WriteBytesCommon(data, length);
}

void DummyPickle::Reserve(size_t length) {
    size_t data_len = AlignUp(length, sizeof(uint32_t));
    if (data_len < length || write_offset_ > std::numeric_limits<uint32_t>::max() - data_len){
        throw std::logic_error("oops:  incorrect data length or write offset!");
    }
    size_t new_size = write_offset_ + data_len;
    if (new_size > capacity_after_header_)
        Resize(capacity_after_header_ * 2 + new_size);
}

void DummyPickle::Resize(size_t new_capacity) {
    capacity_after_header_ = AlignUp<size_t>(new_capacity, PayloadUnit);
    void* p = realloc(header_, GetTotalAllocatedSize());
    if (!p)
        throw std::logic_error("oops: get nullptr");
    header_ = reinterpret_cast<Header*>(p);
}

void *DummyPickle::ClaimBytes(size_t num_bytes) {
    void* p = ClaimUninitializedBytesInternal(num_bytes);
    if (!p)
        throw std::logic_error("oops: get nullptr");
    memset(p, 0, num_bytes);
    return p;
}

const char *DummyPickle::FindNext(size_t header_size, const char *start, const char *end) {
    size_t pickle_size = 0;
    if (!PeekNext(header_size, start, end, &pickle_size))
        return nullptr;

    if (pickle_size > static_cast<size_t>(end - start))
        return nullptr;

    return start + pickle_size;
}

bool DummyPickle::PeekNext(size_t header_size, const char *start, const char *end, size_t *pickle_size) {
    auto length = static_cast<size_t>(end - start);
    if (length < sizeof(Header))
        return false;

    const auto* hdr = reinterpret_cast<const Header*>(start);
    if (length < header_size)
        return false;

    *pickle_size = header_size + hdr->payload_size;
    return true;
}

std::optional<DummyToken> ReadTokenFromPickle(PickleIterator *pickle_iterator) {
    uint64_t high;
    if (!pickle_iterator->ReadUInt64(&high))
        return std::nullopt;

    uint64_t low;
    if (!pickle_iterator->ReadUInt64(&low))
        return std::nullopt;

    return DummyToken{high, low};
}

