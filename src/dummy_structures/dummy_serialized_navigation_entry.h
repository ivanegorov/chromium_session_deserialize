#ifndef SESSION_READER_DUMMY_SERIALIZED_NAVIGATION_ENTRY_H
#define SESSION_READER_DUMMY_SERIALIZED_NAVIGATION_ENTRY_H

#include <vector>
#include <map>

#include "json.h"
#include "dummy_pickle.h"

class DummySerializedNavigationEntry {
public:
    enum BlockedState {
        STATE_INVALID = 0,
        STATE_ALLOWED = 1,
        STATE_BLOCKED = 2,
    };

    enum PasswordState {
        PASSWORD_STATE_UNKNOWN = 0,
        NO_PASSWORD_FIELD = 1,
        HAS_PASSWORD_FIELD = 2,
    };

    DummySerializedNavigationEntry();

    DummySerializedNavigationEntry(const DummySerializedNavigationEntry &other) = default;

    [[maybe_unused]] DummySerializedNavigationEntry(DummySerializedNavigationEntry &&other) noexcept;

    ~DummySerializedNavigationEntry() = default;

    DummySerializedNavigationEntry &operator=(const DummySerializedNavigationEntry &other) = default;

    DummySerializedNavigationEntry &operator=(DummySerializedNavigationEntry &&other) = default;

    void WriteToPickle(int max_size, DummyPickle *pickle) const;

    bool ReadFromPickle(PickleIterator *iterator);

    [[nodiscard]] int index() const { return index_; }

    void set_index(int index) { index_ = index; }

    [[nodiscard]] int unique_id() const { return unique_id_; }

    void set_unique_id(int unique_id) { unique_id_ = unique_id; }

    [[nodiscard]] const std::u16string &title() const { return title_; }

    void set_title(const std::u16string &title) { title_ = title; }

    [[nodiscard]] int http_status_code() const { return http_status_code_; }

    void set_http_status_code(int http_status_code) {
        http_status_code_ = http_status_code;
    }

    [[nodiscard]] bool has_post_data() const { return has_post_data_; }

    [[nodiscard]] int64_t post_id() const { return post_id_; }

    [[nodiscard]] bool is_overriding_user_agent() const { return is_overriding_user_agent_; }

    [[nodiscard]] BlockedState blocked_state() const { return blocked_state_; }

    void set_blocked_state(BlockedState blocked_state) {
        blocked_state_ = blocked_state;
    }

    [[nodiscard]] const std::string& encoded_page_state() const { return encoded_page_state_; }

    [[nodiscard]] PasswordState password_state() const { return password_state_; }

    void set_password_state(PasswordState password_state) {
        password_state_ = password_state;
    }

    [[nodiscard]] bool is_restored() const { return is_restored_; }

    void set_is_restored(bool is_restored) { is_restored_ = is_restored; }

    [[nodiscard]] const std::map<std::string, std::string> &extended_info_map() const {
        return extended_info_map_;
    }

    [[nodiscard]] int64_t task_id() const { return task_id_; }

    void set_task_id(int64_t task_id) { task_id_ = task_id; }

    [[nodiscard]] int64_t parent_task_id() const { return parent_task_id_; }

    void set_parent_task_id(int64_t parent_task_id) {
        parent_task_id_ = parent_task_id;
    }

    [[nodiscard]] int64_t root_task_id() const { return root_task_id_; }

    void set_root_task_id(int64_t root_task_id) { root_task_id_ = root_task_id; }

private:

    friend Json::Node MakeJson(const DummySerializedNavigationEntry & navigator);

    int index_ = -1;

    int unique_id_ = 0;
    std::string referrer_url_str_;
    int referrer_policy_;
    std::u16string title_;
    std::string virtual_url_str_;
    std::string encoded_page_state_;
    int64_t timestamp_;
    int transition_type_ = -1;
    bool has_post_data_ = false;
    int64_t post_id_ = -1;
    std::string original_request_url_str_ {};
    bool is_overriding_user_agent_ = false;
    std::string favicon_url_str_;
    int http_status_code_ = 0;
    bool is_restored_ = false;
    std::vector<std::string> redirect_chain_strs_;  // Not persisted.

    BlockedState blocked_state_ = STATE_INVALID;
    PasswordState password_state_ = PASSWORD_STATE_UNKNOWN;

    std::map<std::string, std::string> extended_info_map_;

    int64_t task_id_ = -1;
    int64_t parent_task_id_ = -1;
    int64_t root_task_id_ = -1;
};

Json::Node MakeJson(DummySerializedNavigationEntry const &);


#endif //SESSION_READER_DUMMY_SERIALIZED_NAVIGATION_ENTRY_H
