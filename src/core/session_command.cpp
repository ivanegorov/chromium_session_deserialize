#include "session_command.h"

Sessions::SessionCommand::SessionCommand(Sessions::SessionCommand::id_type id,
                                         Sessions::SessionCommand::size_type size)
                                         : id_(id), content_(size, 0) {}

bool Sessions::SessionCommand::GetPayload(void *dest, size_t count) const {
    if (Size() != count)
        return false;
    memcpy(dest, &(content_[0]), count);
    return true;
}

std::unique_ptr<DummyPickle> Sessions::SessionCommand::PayloadAsPickle() const {
    return std::make_unique<DummyPickle>(GetContent(), static_cast<int>(Size()));
}