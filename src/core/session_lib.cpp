#include "session_lib.h"

#include <iostream>
#include <utility>
#include <map>

#include "session_command.h"
#include "session_reader.h"

Json::Node Sessions::Deserialize(std::filesystem::path path, bool in_binary) {
    SessionReader reader(std::move(path), {});
    auto res = reader.Read();
    std::cerr << "complete reading" << std::endl;
    return RestoreSessionFromCommands(res.commands, in_binary);
}

void Sessions::Serialize(std::ostream &out, const Json::Node &nodes) {
    Json::Serializer::Serialize(nodes.AsMap(), out);
}

static const Sessions::SessionCommand::id_type CommandSetTabWindow = 0;

static const Sessions::SessionCommand::id_type CommandSetWindowBounds = 1;

static const Sessions::SessionCommand::id_type CommandSetTabIndexInWindow = 2;

static const Sessions::SessionCommand::id_type
        CommandTabNavigationPathPrunedFromBack = 5;

static const Sessions::SessionCommand::id_type CommandUpdateTabNavigation = 6;
static const Sessions::SessionCommand::id_type CommandSetSelectedNavigationIndex = 7;
static const Sessions::SessionCommand::id_type CommandSetSelectedTabInIndex = 8;
static const Sessions::SessionCommand::id_type CommandSetWindowType = 9;

static const Sessions::SessionCommand::id_type CommandSetWindowBounds2 = 10;

static const Sessions::SessionCommand::id_type
        CommandTabNavigationPathPrunedFromFront = 11;

static const Sessions::SessionCommand::id_type CommandSetPinnedState = 12;
static const Sessions::SessionCommand::id_type CommandSetExtensionAppID = 13;
static const Sessions::SessionCommand::id_type CommandSetWindowBounds3 = 14;
static const Sessions::SessionCommand::id_type CommandSetWindowAppName = 15;
static const Sessions::SessionCommand::id_type CommandTabClosed = 16;
static const Sessions::SessionCommand::id_type CommandWindowClosed = 17;

static const Sessions::SessionCommand::id_type CommandSetTabUserAgentOverride = 18;
static const Sessions::SessionCommand::id_type CommandSessionStorageAssociated = 19;
static const Sessions::SessionCommand::id_type CommandSetActiveWindow = 20;
static const Sessions::SessionCommand::id_type CommandLastActiveTime = 21;

static const Sessions::SessionCommand::id_type CommandSetWindowWorkspace = 22;

static const Sessions::SessionCommand::id_type CommandSetWindowWorkspace2 = 23;
static const Sessions::SessionCommand::id_type CommandTabNavigationPathPruned = 24;
static const Sessions::SessionCommand::id_type CommandSetTabGroup = 25;

static const Sessions::SessionCommand::id_type CommandSetTabGroupMetadata = 26;

static const Sessions::SessionCommand::id_type CommandSetTabGroupMetadata2 = 27;
static const Sessions::SessionCommand::id_type CommandSetTabGuid = 28;
static const Sessions::SessionCommand::id_type CommandSetTabUserAgentOverride2 = 29;
static const Sessions::SessionCommand::id_type CommandSetTabData = 30;
static const Sessions::SessionCommand::id_type CommandSetWindowUserTitle = 31;
static const Sessions::SessionCommand::id_type CommandSetWindowVisibleOnAllWorkspaces =
        32;
static const Sessions::SessionCommand::id_type CommandAddTabExtraData = 33;
static const Sessions::SessionCommand::id_type CommandAddWindowExtraData = 34;

std::map<Sessions::SessionCommand::id_type, std::string> id_by_command_{
        {CommandSetTabWindow,                     "Set tab window"},
        {CommandSetWindowBounds,                  "Set window bounds"},
        {CommandSetTabIndexInWindow,              "Set tab index in window"},
        {CommandTabNavigationPathPrunedFromBack,  "Tab navigation path pruned from back"},
        {CommandUpdateTabNavigation,              "Update tab navigation"},
        {CommandSetSelectedNavigationIndex,       "Set selected navigation index"},
        {CommandSetSelectedTabInIndex,            "Set selected tab index"},
        {CommandSetWindowType,                    "Set window type"},
        {CommandSetWindowBounds2,                 "Set window bounds2"},
        {CommandTabNavigationPathPrunedFromFront, "Tab navigation path pruned from font"},
        {CommandSetPinnedState,                   "Set pinned state"},
        {CommandSetExtensionAppID,                "Extension app ID"},
        {CommandSetWindowBounds3,                 "Set window bounds3"},
        {CommandSetWindowAppName,                 "Set window app name"},
        {CommandTabClosed,                        "Tab closed"},
        {CommandWindowClosed,                     "Window closed"},
        {CommandSetTabUserAgentOverride,          "Set tab user agent override"},
        {CommandSessionStorageAssociated,         "Session storage associated"},
        {CommandSetActiveWindow,                  "Set active window"},
        {CommandLastActiveTime,                   "Last active time"},
        {CommandSetWindowWorkspace,               "Set window workspace"},
        {CommandSetWindowWorkspace2,              "Set window workspace 2"},
        {CommandTabNavigationPathPruned,          "Tab navigation path pruned"},
        {CommandSetTabGroup,                      "Set tab group"},
        {CommandSetTabGroupMetadata,              "Set tab group metadata"},
        {CommandSetTabGroupMetadata2,             "Set tab group metadata2"},
        {CommandSetTabGuid,                       "Set tab guid"},
        {CommandSetTabUserAgentOverride2,         "Set tab user agent override2"},
        {CommandSetTabData,                       "Set tab data"},
        {CommandSetWindowUserTitle,               "Set window user title"},
        {CommandSetWindowVisibleOnAllWorkspaces,  "Set window visible on all workspaces"},
        {CommandAddTabExtraData,                  "Add tab extra data"},
        {CommandAddWindowExtraData,               "Add window extra data"}
};


#define GetPayloadMacro(payload)                                                                    \
    if (!command->GetPayload(&(payload), sizeof(payload))) {                                           \
        std::cerr << "Failed reading command " << id_by_command_.at(command->GetId())               \
             <<  " because size of payload of command " << command->Size()                          \
             << " not equal to sizeof payload structure " << sizeof(payload) <<  std::endl;         \
        break;                                                                                      \
    }

Json::Node Sessions::RestoreSessionFromCommands(
        const std::vector<std::unique_ptr<Sessions::SessionCommand>> &commands, bool in_binary) {
    Json::Node result;
    std::map<Sessions::SessionCommand::id_type, size_t> count;

    auto &commands_dict = result.Insert("Commands", Json::Node{});

    for (const auto &command_ptr: commands) {
        const Sessions::SessionCommand *command = command_ptr.get();

        if (id_by_command_.find(command->GetId()) == id_by_command_.end())
            continue;

        if (in_binary) {
            std::string payload(command->GetContent(), command->Size());
            commands_dict.Insert(id_by_command_[command->GetId()] + " #" + std::to_string(count[command->GetId()]++),
                                 string_to_hex(payload));
        } else {
            auto &cur_node = commands_dict.Insert(
                    id_by_command_[command->GetId()] + " #" + std::to_string(count[command->GetId()]++), Json::Node{});

            switch (command->GetId()) {
                case CommandSetTabWindow: {
                    SessionID::id_type payload[2];
                    GetPayloadMacro(payload);
                    cur_node.Insert("window id", payload[0]);
                    cur_node.Insert("tab id", payload[1]);
                    break;
                }
                case CommandSetWindowBounds2: {
                    WindowBoundsPayload2 payload{};
                    GetPayloadMacro(payload);
                    cur_node = MakeJson(payload);
                    break;
                }
                case CommandSetWindowBounds:
                case CommandSetWindowBounds3: {
                    WindowBoundsPayload3 payload{};
                    GetPayloadMacro(payload);
                    cur_node = MakeJson(payload);
                    break;
                }
                case CommandSetTabIndexInWindow: {
                    TabIndexInWindowPayload payload;
                    GetPayloadMacro(payload);
                    cur_node = MakeJson(payload);
                    break;
                }
                case CommandTabClosed:
                case CommandWindowClosed: {
                    ClosedPayload payload{};
                    GetPayloadMacro(payload);
                    cur_node = MakeJson(payload);
                    break;
                }
                case CommandTabNavigationPathPrunedFromBack: {
                    TabNavigationPathPrunedFromBackPayload payload;
                    GetPayloadMacro(payload);
                    cur_node = MakeJson(payload);
                    break;
                }
                case CommandTabNavigationPathPrunedFromFront: {
                    TabNavigationPathPrunedFromFrontPayload prune_front_payload;
                    if (!command->GetPayload(&prune_front_payload,
                                             sizeof(prune_front_payload)) ||
                        prune_front_payload.index <= 0) {
                        std::cerr << "Failed reading command " << id_by_command_.at(command->GetId());
                        break;
                    }
                    cur_node.Insert("TabNavigationPathPrunedFromFrontPayload", MakeJson(prune_front_payload));

                    TabNavigationPathPrunedPayload payload{};
                    payload.index = 0;
                    payload.count = prune_front_payload.index;
                    cur_node.Insert("TabNavigationPathPrunedPayload", MakeJson(payload));
                    break;
                }
                case CommandTabNavigationPathPruned: {
                    TabNavigationPathPrunedPayload payload{};
                    if (!command->GetPayload(&payload, sizeof(payload)) ||
                        payload.index < 0 || payload.count <= 0) {
                        std::cerr << "Failed reading command " << id_by_command_.at(command->GetId());
                        break;
                    }
                    cur_node = MakeJson(payload);
                    break;
                }
                case CommandUpdateTabNavigation: {
                    DummySerializedNavigationEntry navigation;
                    SessionID tab_id = Sessions::SessionID::FromSerializedValue(-1);
                    if (!RestoreUpdateTabNavigationCommand(*command,
                                                           &navigation,
                                                           &tab_id)) {
                        std::cerr << "Failed reading command " << id_by_command_.at(command->GetId());
                        break;
                    }
                    cur_node.Insert("Serialized navigation entry", MakeJson(navigation));
                    cur_node.Insert("tab_id", tab_id.GetId());
                    break;
                }
                case CommandSetSelectedNavigationIndex: {
                    SelectedNavigationIndexPayload payload{};
                    GetPayloadMacro(payload);
                    cur_node = MakeJson(payload);
                    break;
                }
                case CommandSetSelectedTabInIndex: {
                    SelectedTabInIndexPayload payload;
                    GetPayloadMacro(payload);
                    cur_node = MakeJson(payload);
                    break;
                }
                case CommandSetWindowType: {
                    WindowTypePayload payload;
                    GetPayloadMacro(payload);
                    cur_node = MakeJson(payload);
                    break;
                }
                case CommandSetTabGroup: {
                    TabGroupPayload payload{};
                    GetPayloadMacro(payload);
                    cur_node = MakeJson(payload);
                    break;
                }
                case CommandSetTabGroupMetadata2: {
                    std::unique_ptr<DummyPickle> pickle = command->PayloadAsPickle();
                    PickleIterator iter(*pickle);

                    std::optional<DummyToken> group_token = ReadTokenFromPickle(&iter);
                    if (!group_token.has_value()) {
                        std::cerr << "Failed reading command " << id_by_command_.at(command->GetId());
                        break;
                    }

                    std::u16string title;
                    if (!iter.ReadString16(&title)) {
                        std::cerr << "Failed reading command " << id_by_command_.at(command->GetId());
                        break;
                    }

                    uint32_t color_int;
                    if (!iter.ReadUInt32(&color_int)) {
                        std::cerr << "Failed reading command " << id_by_command_.at(command->GetId());
                        break;
                    }

                    bool is_collapsed = false;
                    std::ignore = iter.ReadBool(&is_collapsed);

                    cur_node.Insert("title", title);
                    cur_node.Insert("color int", color_int);
                    cur_node.Insert("is collapsed", is_collapsed);

                    break;
                }
                case CommandSetPinnedState: {
                    PinnedStatePayload payload{};
                    GetPayloadMacro(payload);
                    cur_node = MakeJson(payload);
                    break;
                }
                case CommandSetWindowAppName: {
                    SessionID window_id = SessionID::FromSerializedValue(-1);
                    std::string app_name;
                    if (!RestoreSetWindowAppNameCommand(*command, &window_id, &app_name)) {
                        std::cerr << "Failed reading command " << id_by_command_.at(command->GetId());
                        break;
                    }

                    cur_node.Insert("window id", window_id.GetId());
                    cur_node.Insert("app name", app_name);

                    break;
                }
                case CommandSetExtensionAppID: {
                    SessionID tab_id = SessionID::FromSerializedValue(-1);
                    std::string extension_app_id;
                    if (!RestoreSetTabExtensionAppIDCommand(*command,
                                                            &tab_id,
                                                            &extension_app_id)) {
                        std::cerr << "Failed reading command " << id_by_command_.at(command->GetId());
                        break;
                    }

                    cur_node.Insert("tab id", tab_id.GetId());
                    cur_node.Insert("extension app id", extension_app_id);

                    break;
                }
                case CommandSetTabUserAgentOverride: {
                    SessionID tab_id = SessionID::FromSerializedValue(-1);
                    std::string user_agent_override;
                    if (!RestoreSetTabUserAgentOverrideCommand(
                            *command,
                            &tab_id,
                            &user_agent_override)) {
                        std::cerr << "Failed reading command " << id_by_command_.at(command->GetId());
                        break;
                    }

                    cur_node.Insert("tab id", tab_id.GetId());
                    cur_node.Insert("user agent override", user_agent_override);

                    break;
                }
                case CommandSetTabUserAgentOverride2: {
                    SessionID tab_id = SessionID::FromSerializedValue(-1);
                    std::string user_agent_override;
                    std::optional<std::string> opaque_ua_metadata_override;
                    if (!RestoreSetTabUserAgentOverrideCommand2(
                            *command, &tab_id, &user_agent_override,
                            &opaque_ua_metadata_override)) {
                        break;
                    }

                    cur_node.Insert("tab id", tab_id.GetId());
                    cur_node.Insert("user agent override", user_agent_override);
                    cur_node.Insert("opaque ua metadata override", *opaque_ua_metadata_override);

                    break;
                }
                case CommandSessionStorageAssociated: {
                    std::unique_ptr<DummyPickle> command_pickle(
                            command->PayloadAsPickle());
                    SessionID::id_type command_tab_id;
                    std::string session_storage_persistent_id;
                    PickleIterator iter(*command_pickle);
                    if (!iter.ReadInt(&command_tab_id) ||
                        !iter.ReadString(&session_storage_persistent_id))
                        break;

                    cur_node.Insert("command tab id", command_tab_id);
                    cur_node.Insert("session storage persistent id", session_storage_persistent_id);

                    break;
                }
                case CommandSetActiveWindow: {
                    ActiveWindowPayload payload;
                    GetPayloadMacro(payload);
                    cur_node.Insert("active window id", payload);
                    break;
                }
                case CommandLastActiveTime: {
                    LastActiveTimePayload payload{};
                    GetPayloadMacro(payload);
                    cur_node = MakeJson(payload);
                    break;
                }
                case CommandSetWindowWorkspace2: {
                    std::unique_ptr<DummyPickle> pickle(command->PayloadAsPickle());
                    PickleIterator it(*pickle);
                    SessionID::id_type window_id = -1;
                    std::string workspace;
                    if (!it.ReadInt(&window_id) || !it.ReadString(&workspace)) {
                        std::cerr << "Failed reading command " << id_by_command_.at(command->GetId());
                        break;
                    }

                    cur_node.Insert("window id", window_id);
                    cur_node.Insert("workspace", workspace);

                    break;
                }
                case CommandSetWindowVisibleOnAllWorkspaces: {
                    VisibleOnAllWorkspacesPayload payload{};
                    GetPayloadMacro(payload);
                    cur_node = MakeJson(payload);
                    break;
                }
                case CommandSetTabGuid: {
                    std::unique_ptr<DummyPickle> pickle(command->PayloadAsPickle());
                    PickleIterator it(*pickle);
                    SessionID::id_type tab_id = -1;
                    std::string guid;
                    if (!it.ReadInt(&tab_id) || !it.ReadString(&guid)) {
                        std::cerr << "Failed reading command " << id_by_command_.at(command->GetId());
                        break;
                    }

                    cur_node.Insert("tab id", tab_id);
                    cur_node.Insert("guid", guid);

                    break;
                }
                case CommandSetTabData: {
                    std::unique_ptr<DummyPickle> pickle(command->PayloadAsPickle());
                    PickleIterator it(*pickle);
                    SessionID::id_type tab_id = -1;
                    int size = 0;
                    if (!it.ReadInt(&tab_id) || !it.ReadInt(&size)) {
                        std::cerr << "Failed reading command " << id_by_command_.at(command->GetId());
                        break;
                    }
                    std::map<std::string, Json::Node> tab_data;
                    for (int i = 0; i < size; i++) {
                        std::string key;
                        std::string value;
                        if (!it.ReadString(&key) || !it.ReadString(&value)) {
                            std::cerr << "Failed reading command " << id_by_command_.at(command->GetId());
                            break;
                        }
                        tab_data.insert({key, Json::Node{value}});
                    }

                    cur_node.Insert("tab id", tab_id);
                    cur_node.Insert("size", size);
                    cur_node.Insert("tab data", tab_data);

                    break;
                }
                case CommandAddTabExtraData: {
                    std::unique_ptr<DummyPickle> pickle(command->PayloadAsPickle());
                    PickleIterator it(*pickle);

                    SessionID tab_id = SessionID::FromSerializedValue(-1);
                    std::string key;
                    std::string data;
                    if (!ReadSessionIdFromPickle(&it, &tab_id) || !it.ReadString(&key) ||
                        !it.ReadString(&data)) {
                        std::cerr << "Failed reading command " << id_by_command_.at(command->GetId());
                        break;
                    }

                    cur_node.Insert("tab id", tab_id.GetId());
                    cur_node.Insert(key, data);

                    break;
                }
                case CommandAddWindowExtraData: {
                    std::unique_ptr<DummyPickle> pickle(command->PayloadAsPickle());
                    PickleIterator it(*pickle);

                    SessionID window_id = SessionID::FromSerializedValue(-1);
                    std::string key;
                    std::string data;
                    if (!ReadSessionIdFromPickle(&it, &window_id) || !it.ReadString(&key) ||
                        !it.ReadString(&data)) {
                        std::cerr << "Failed reading command " << id_by_command_.at(command->GetId());
                        break;
                    }

                    cur_node.Insert("window id", window_id.GetId());
                    cur_node.Insert(key, data);

                    break;
                }
                case CommandSetWindowUserTitle: {
                    SessionID window_id = SessionID::FromSerializedValue(-1);
                    std::string title;
                    if (!RestoreSetWindowUserTitleCommand(*command, &window_id, &title)) {
                        std::cerr << "Failed reading command " << id_by_command_.at(command->GetId());
                        break;
                    }

                    cur_node.Insert("window id", window_id.GetId());
                    cur_node.Insert("title", title);

                    break;
                }
                default:
                    std::cerr << "Failed reading an unknown command by id " << command->GetId() << std::endl;
                    break;
            }
        }
    }
    return std::move(result);
}

bool Sessions::ReadSessionIdFromPickle(PickleIterator *iterator, Sessions::SessionID *id) {
    SessionID::id_type value;
    if (!iterator->ReadInt(&value)) {
        return false;
    }
    *id = SessionID::FromSerializedValue(value);
    return true;
}

bool Sessions::RestoreUpdateTabNavigationCommand(const Sessions::SessionCommand &command,
                                                 DummySerializedNavigationEntry *navigation,
                                                 Sessions::SessionID *tab_id) {
    std::unique_ptr<DummyPickle> pickle(command.PayloadAsPickle());
    if (!pickle)
        return false;
    PickleIterator iterator(*pickle);
    return ReadSessionIdFromPickle(&iterator, tab_id) &&
           navigation->ReadFromPickle(&iterator);
}

bool Sessions::RestoreSetWindowAppNameCommand(const Sessions::SessionCommand &command, Sessions::SessionID *window_id,
                                              std::string *app_name) {
    std::unique_ptr<DummyPickle> pickle(command.PayloadAsPickle());
    if (!pickle)
        return false;

    PickleIterator iterator(*pickle);
    return ReadSessionIdFromPickle(&iterator, window_id) &&
           iterator.ReadString(app_name);
}

bool Sessions::RestoreSetTabExtensionAppIDCommand(const Sessions::SessionCommand &command, Sessions::SessionID *tab_id,
                                                  std::string *extension_app_id) {
    std::unique_ptr<DummyPickle> pickle(command.PayloadAsPickle());
    if (!pickle)
        return false;

    PickleIterator iterator(*pickle);
    return ReadSessionIdFromPickle(&iterator, tab_id) &&
           iterator.ReadString(extension_app_id);
}

bool
Sessions::RestoreSetTabUserAgentOverrideCommand2(const Sessions::SessionCommand &command, Sessions::SessionID *tab_id,
                                                 std::string *user_agent_override,
                                                 std::optional<std::string> *opaque_ua_metadata_override) {
    std::unique_ptr<DummyPickle> pickle(command.PayloadAsPickle());
    if (!pickle)
        return false;

    PickleIterator iterator(*pickle);
    if (!ReadSessionIdFromPickle(&iterator, tab_id))
        return false;
    if (!iterator.ReadString(user_agent_override))
        return false;

    bool has_ua_metadata_override;
    if (!iterator.ReadBool(&has_ua_metadata_override))
        return false;
    if (!has_ua_metadata_override) {
        *opaque_ua_metadata_override = std::nullopt;
        return true;
    }

    std::string ua_metadata_override_value;
    if (!iterator.ReadString(&ua_metadata_override_value))
        return false;

    *opaque_ua_metadata_override = std::move(ua_metadata_override_value);
    return true;
}

bool
Sessions::RestoreSetTabUserAgentOverrideCommand(const Sessions::SessionCommand &command, Sessions::SessionID *tab_id,
                                                std::string *user_agent_override) {
    std::unique_ptr<DummyPickle> pickle(command.PayloadAsPickle());
    if (!pickle)
        return false;

    PickleIterator iterator(*pickle);
    return ReadSessionIdFromPickle(&iterator, tab_id) &&
           iterator.ReadString(user_agent_override);
}

bool Sessions::RestoreSetWindowUserTitleCommand(const Sessions::SessionCommand &command, Sessions::SessionID *window_id,
                                                std::string *user_title) {
    std::unique_ptr<DummyPickle> pickle(command.PayloadAsPickle());
    if (!pickle)
        return false;

    PickleIterator iterator(*pickle);
    return ReadSessionIdFromPickle(&iterator, window_id) &&
           iterator.ReadString(user_title);
}