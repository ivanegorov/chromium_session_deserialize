#ifndef SESSION_READER_SESSION_COMMAND_H
#define SESSION_READER_SESSION_COMMAND_H

#include <cstdint>
#include <string>

#include <memory>

#include "dummy_structures/dummy_pickle.h"

namespace Sessions {
    struct SessionID {
        using id_type = int32_t;

        static constexpr SessionID FromSerializedValue(id_type value) {
            return SessionID{value};
        }

        [[nodiscard]] id_type GetId() const { return id_; }

    private:
        explicit constexpr SessionID(id_type id) : id_(id) {}

        id_type id_;
    };

    inline bool operator==(SessionID lhs, SessionID rhs) {
        return lhs.GetId() == rhs.GetId();
    }

    inline bool operator!=(SessionID lhs, SessionID rhs) {
        return lhs.GetId() != rhs.GetId();
    }

    inline bool operator<(SessionID lhs, SessionID rhs) {
        return lhs.GetId() < rhs.GetId();
    }

    struct SessionCommand {
    public:
        using id_type = uint8_t;

        using size_type = uint16_t;

        SessionCommand(id_type id, size_type size);

        [[nodiscard]] uint32_t GetId() const { return id_; }

        [[nodiscard]] const char *GetContent() const { return content_.data(); }

        char *GetContent() { return content_.data(); }

        bool GetPayload(void *dest, size_t count) const;

        std::unique_ptr<DummyPickle> PayloadAsPickle() const;

        [[nodiscard]] size_type Size() const { return static_cast<size_type>(content_.size()); }

    private:
        uint8_t id_;
        std::string content_;
    };
}


#endif //SESSION_READER_SESSION_COMMAND_H
