#include "session_reader.h"

#include <iostream>

const int32_t Sessions::FileSignature = ArrayToValue<int32_t>(reinterpret_cast<const uint8_t *>("SNSS"));

const int32_t Sessions::InitialStateMarkerCommandId = 255;

const uint32_t Sessions::BufferSize = 1024;

Sessions::SessionReader::SessionReader(std::filesystem::path path, const std::vector<uint8_t> &crypto_key)
        : buffer_(BufferSize, 0) {
    if (!crypto_key.empty())
        throw std::logic_error("is not supported by aaid yet");

    file_.path = std::move(path);
    file_.inp.open(file_.path, std::ios::binary);

//    std::cerr << file_.inp.rdbuf();

    is_header_valid_ = ReadHeader();
}

Sessions::SessionReader::AlreadyReadCommands Sessions::SessionReader::Read() {
    if (!IsHeaderValid())
        return {};

    AlreadyReadCommands commands_result;
    bool error_reading = false;
    for (ReadResult result = ReadCommand(); result.command; result = ReadCommand()) {
        if (result.command->GetId() != InitialStateMarkerCommandId) {
            commands_result.error_reading += error_reading;
            commands_result.commands.push_back(std::move(result.command));
        }
    }
    return std::move(commands_result);
}

bool Sessions::SessionReader::ReadHeader() {
    if (did_check_header_)
        return is_header_valid_;
    did_check_header_ = true;

    if (!file_.inp.is_open()) {
        throw std::logic_error(std::string("Can't open file ") + file_.path.string());
    }
    FileHeader header{};
    const size_t read_count = file_.inp.readsome(reinterpret_cast<char *>(&header), sizeof(header));
    if (read_count != sizeof(header) || header.signature != FileSignature)
        return false;
    version_ = header.version;
    return (version_ == FILE_VERSION::FileVersion1 ||
            version_ == FILE_VERSION::FileVersionWithMarker);
}

Sessions::SessionReader::ReadResult Sessions::SessionReader::ReadCommand() {
    ReadResult cur_result;

    if (available_count_ < sizeof(SessionCommand::size_type)) {
        if (!FillBuffer())
            return std::move(cur_result);
        if (available_count_ < sizeof(SessionCommand::size_type)) {
            cur_result.error_reading = true;
            return std::move(cur_result);
        }
    }

    SessionCommand::size_type command_size;
    memcpy(&command_size, &(buffer_[buffer_pos_]), sizeof(command_size));
    buffer_pos_ += sizeof(command_size);
    available_count_ -= sizeof(command_size);
    if (command_size == 0) {
        cur_result.error_reading = true;
        return std::move(cur_result);
    }

    if (command_size > available_count_) {
        if (command_size > buffer_.size())
            buffer_.resize((command_size / 1024 + 1) * 1024, 0);
        if (!FillBuffer() || command_size > available_count_) {
            cur_result.error_reading = true;
            return std::move(cur_result);
        }
    }

    cur_result.command =
            CreateCommand(buffer_.c_str() + buffer_pos_, command_size);

    ++command_counter_;
    buffer_pos_ += command_size;
    available_count_ -= command_size;
    return std::move(cur_result);
}

std::unique_ptr<Sessions::SessionCommand> Sessions::SessionReader::CreateCommand(
        const char * data, SessionCommand::size_type size) {
    const SessionCommand::id_type id = data[0];
    std::unique_ptr<Sessions::SessionCommand> command =
            std::make_unique<Sessions::SessionCommand>(id,
                                                       size - sizeof(SessionCommand::id_type));
    if (size > sizeof(SessionCommand::id_type)) {
        memcpy(command->GetContent(), &(data[sizeof(SessionCommand::id_type)]),
               size - sizeof(SessionCommand::id_type));
    }
    return std::move(command);
}

bool Sessions::SessionReader::HasMarker() {
    if (!IsSupportMarker())
        return false;
    for (ReadResult result = ReadCommand(); result.command; result = ReadCommand()) {
        if (result.command->GetId() == InitialStateMarkerCommandId) {
            return true;
        }
    }
    return false;
}

bool Sessions::SessionReader::FillBuffer() {
    if (available_count_ > 0 && buffer_pos_ > 0) {
        memmove(&(buffer_[0]), &(buffer_[buffer_pos_]), available_count_);
    }
    buffer_pos_ = 0;
    int to_read = static_cast<int>(buffer_.size() - available_count_);
    size_t read_count =
            file_.inp.readsome(&(buffer_[available_count_]), to_read);
    if (read_count == 0) {
        return false;
    }
    available_count_ += read_count;
    return true;
}