#ifndef SESSION_READER_SESSION_READER_H
#define SESSION_READER_SESSION_READER_H

#include <memory>
#include <filesystem>
#include <fstream>
#include <cstdint>

#include "auxiliary.h"
#include "session_command.h"

namespace Sessions {
    enum FILE_VERSION : uint8_t {
        FileVersion1 = 1,
        EncryptedFileVersion = 2,
        FileVersionWithMarker = 3,
        EncryptedFileVersionWithMarker = 4
    };

    extern const int32_t FileSignature;

    extern const int32_t InitialStateMarkerCommandId;

    extern const uint32_t BufferSize;

    struct FileHeader {
        int32_t signature;
        FILE_VERSION version;
    };

    struct SessionReader {
    public:
        struct ReadResult {
            std::unique_ptr<SessionCommand> command;
            bool error_reading = false;
        };

        struct AlreadyReadCommands {
            std::vector<std::unique_ptr<SessionCommand>> commands;
            bool error_reading = false;
        };

        explicit SessionReader(std::filesystem::path path, const std::vector<uint8_t> &crypto_key);

        bool IsHeaderValid() const { return is_header_valid_; }

        bool IsSupportMarker() const {
            return IsHeaderValid() && (version_ == FILE_VERSION::FileVersionWithMarker ||
                                       version_ == FILE_VERSION::EncryptedFileVersionWithMarker);
        }

        std::filesystem::path GetPath() const { return file_.path; }

        AlreadyReadCommands Read();

        static std::unique_ptr<Sessions::SessionCommand> CreateCommand(const char *, SessionCommand::size_type size);

    private:
        bool ReadHeader();

        ReadResult ReadCommand();

        bool HasMarker();

        bool FillBuffer();

        struct FileInfo {
            std::ifstream inp;
            std::filesystem::path path;
        } file_;

        bool is_header_valid_ = false;

        bool did_check_header_ = false;

        FILE_VERSION version_;

        std::string buffer_;

        size_t buffer_pos_ = 0;

        size_t available_count_ = 0;

        int command_counter_ = 0;

    };
}


#endif //SESSION_READER_SESSION_READER_H
