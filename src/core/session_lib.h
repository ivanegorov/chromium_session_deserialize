#ifndef SESSION_READER_SESSION_LIB_H
#define SESSION_READER_SESSION_LIB_H

#include <memory>
#include <filesystem>
#include <fstream>
#include <cstdint>
#include <type_traits>

#include <boost/hana/assert.hpp>
#include <boost/hana/at_key.hpp>
#include <boost/hana/core/to.hpp>
#include <boost/hana/define_struct.hpp>
#include <boost/hana/for_each.hpp>
#include <boost/hana/keys.hpp>
#include <boost/hana/pair.hpp>

#include "auxiliary.h"
#include "session_command.h"
#include "dummy_structures/dummy_pickle.h"
#include "dummy_structures/dummy_serialized_navigation_entry.h"

#include "json.h"

namespace bh = boost::hana;

namespace Sessions {
    Json::Node Deserialize(std::filesystem::path path, bool in_binary = false);

    void Serialize(std::ostream &out, const Json::Node &dict);

    Json::Node
    RestoreSessionFromCommands(const std::vector<std::unique_ptr<Sessions::SessionCommand>> &commands, bool in_binary = false);

    template<typename T>
    std::enable_if_t<!bh::Struct<T>::value,
            Json::Node> MakeJson(const T & payload) {
        return {payload};
    }

    template<typename T>
    std::enable_if_t<bh::Struct<T>::value,
            Json::Node> MakeJson(T const &payload) {
        Json::Node node{};
        boost::hana::for_each(bh::keys(payload), [&](auto key) {
            auto &member = bh::at_key(payload, key);
            node.Insert(std::string(key.c_str()), MakeJson(member));
        });
        return std::move(node);
    }

    struct WindowBoundsPayload2 {
        BOOST_HANA_DEFINE_STRUCT(WindowBoundsPayload2,
                                 (SessionID::id_type, window_id),
                                 (int32_t, x),
                                 (int32_t, y),
                                 (int32_t, w),
                                 (int32_t, h),
                                 (bool, is_maximized));
    };

    struct WindowBoundsPayload3 {
        BOOST_HANA_DEFINE_STRUCT(WindowBoundsPayload3,
                                 (SessionID::id_type, window_id),
                                 (int32_t, x),
                                 (int32_t, y),
                                 (int32_t, w),
                                 (int32_t, h),
                                 (int32_t, show_state));
    };

    using ActiveWindowPayload = SessionID::id_type;

    struct IDAndIndexPayload {
        BOOST_HANA_DEFINE_STRUCT(IDAndIndexPayload,
                                 (SessionID::id_type, id),
                                 (int32_t, index)
        );
    };

    using TabIndexInWindowPayload = IDAndIndexPayload;

    using TabNavigationPathPrunedFromBackPayload = IDAndIndexPayload;

    using SelectedNavigationIndexPayload = IDAndIndexPayload;

    using SelectedTabInIndexPayload = IDAndIndexPayload;

    using WindowTypePayload = IDAndIndexPayload;

    using TabNavigationPathPrunedFromFrontPayload = IDAndIndexPayload;

    struct ClosedPayload {
        BOOST_HANA_DEFINE_STRUCT(ClosedPayload,
                                 (SessionID::id_type, id),
                                 (int64_t, close_time)
        );
    };

    struct TabNavigationPathPrunedPayload {
        BOOST_HANA_DEFINE_STRUCT(TabNavigationPathPrunedPayload,
                                 (SessionID::id_type, id),
        // Index starting which |count| entries were removed.
                                 (int32_t, index),
        // Number of entries removed.
                                 (int32_t, count));
    };

    struct SerializedToken {
        BOOST_HANA_DEFINE_STRUCT(SerializedToken,
        // These fields correspond to the high and low fields of |base::Token|.
                                 (uint64_t, id_high),
                                 (uint64_t, id_low));
    };

    struct TabGroupPayload {
        BOOST_HANA_DEFINE_STRUCT(TabGroupPayload,
                                 (SessionID::id_type, tab_id),
                                 (SerializedToken, maybe_group),
                                 (bool, has_group));
    };

    struct PinnedStatePayload {
        BOOST_HANA_DEFINE_STRUCT(PinnedStatePayload,
                                 (SessionID::id_type, tab_id),
                                 (bool, pinned_state));
    };

    struct LastActiveTimePayload {
        BOOST_HANA_DEFINE_STRUCT(LastActiveTimePayload,
                                 (SessionID::id_type, tab_id),
                                 (int64_t, last_active_time));
    };

    struct VisibleOnAllWorkspacesPayload {
        BOOST_HANA_DEFINE_STRUCT(VisibleOnAllWorkspacesPayload,
                                 (SessionID::id_type, window_id),
                                 (bool, visible_on_all_workspaces));
    };

    bool ReadSessionIdFromPickle(PickleIterator* iterator, SessionID* id);

    bool RestoreUpdateTabNavigationCommand(
            const SessionCommand& command,
            DummySerializedNavigationEntry* navigation,
            SessionID* tab_id);

    bool RestoreSetWindowAppNameCommand(const SessionCommand& command,
                                        SessionID* window_id,
                                        std::string* app_name);

    bool RestoreSetTabExtensionAppIDCommand(const SessionCommand& command,
                                            SessionID* tab_id,
                                            std::string* extension_app_id);

    bool RestoreSetTabUserAgentOverrideCommand(const SessionCommand& command,
                                               SessionID* tab_id,
                                               std::string* user_agent_override);

    bool RestoreSetTabUserAgentOverrideCommand2(
            const SessionCommand& command,
            SessionID* tab_id,
            std::string* user_agent_override,
            std::optional<std::string>* opaque_ua_metadata_override);

    bool RestoreSetWindowUserTitleCommand(const SessionCommand& command,
                                          SessionID* window_id,
                                          std::string* user_title);
}

#endif //SESSION_READER_SESSION_LIB_H
