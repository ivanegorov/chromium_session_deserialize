#ifndef SESSION_READER_AUXILIARY_H
#define SESSION_READER_AUXILIARY_H

#include <stdexcept>
#include <vector>
#include <cstdint>

template <typename T, typename = std::enable_if_t<std::is_integral_v<T>>>
constexpr T AlignUp(T size, T alignment) {
    if (!(alignment > 0 && ((alignment & (alignment-1)) == 0))){
        throw std::logic_error("oops: align up to not power of two");
    }
    return (size + alignment - 1) & ~(alignment - 1);
}

template <typename T, typename = typename std::enable_if<sizeof(T) == 1>::type>
inline T* AlignUp(T* ptr, uintptr_t alignment) {
    return reinterpret_cast<T*>(
            AlignUp(reinterpret_cast<uintptr_t>(ptr), alignment));
}

template <typename T> void ValueToArray(T value, uint8_t *arr) {
    union value_type {
        T full;
        unsigned char u8[sizeof(T)];
    } smart_value{value};
    memcpy(arr, &smart_value.u8, sizeof(T));
}

template <typename T> T ArrayToValue(const uint8_t *arr) {
    union value_type {
        T full;
        unsigned char u8[sizeof(T)];
    } smart_value;
    for (size_t i = 0; i < sizeof(T); i++) {
        smart_value.u8[i] = arr[i];
    }
    return smart_value.full;
}

std::string string_to_hex(const std::string& input);

std::vector<int> u16string_to_hex(const std::u16string& input);

#endif //SESSION_READER_AUXILIARY_H
