#include <iostream>
#include <fstream>

#include "core/session_lib.h"

int main(int argc, char **argv) {
    if (argc < 2) {
        std::cerr << "USAGE: session_reader <Path> [optional]<Is binary output> = {true/false}";
        return 1;
    }

    std::ofstream out(std::string(argv[1]) + std::string(".json"), std::ios::binary);
    if (!out.is_open()) {
        std::cerr << "Can't create file " << std::string(argv[1]) << std::endl;
        return 1;
    }

    Sessions::Serialize(out, Sessions::Deserialize(std::filesystem::path{std::string(argv[1])},
                                                   (std::string(argv[2]) == std::string("true"))));

    return 0;
}
